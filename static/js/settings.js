"use strict";
/*
 ********************************************************************************************
 **    Teeworlds Web Panel
 **    Copyright (C) 2016-2017  Alexandre Díaz
 **
 **    This program is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU Affero General Public License as
 **    published by the Free Software Foundation, either version 3 of the
 **    License.
 **
 **    This program is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU Affero General Public License for more details.
 **
 **    You should have received a copy of the GNU Affero General Public License
 **    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ********************************************************************************************
 */
//TODO: Change concats to formatted strings
$(function(){

	$(document).on('click', "#form-settings-change-pass button[id='btn-submit']", function(e){
		var passOld = $('#pass-old').val();
		var passNew = $('#pass-new').val();
		var passNewRepeat = $('#pass-new-repeat').val();

		// Validate Form
		$('#pass-old,#pass-new,#pass-new-repeat').tooltip('hide');
		if (!passOld)
		{
			$('#pass-old').tooltip({trigger:'manual', placement:'bottom', title:$BABEL_STR_CANT_BE_EMPTY});
			$('#pass-old').tooltip('show');
			return;
		}
		if (!passNew)
		{
			$('#pass-new').tooltip({trigger:'manual', placement:'bottom', title:$BABEL_STR_CANT_BE_EMPTY});
			$('#pass-new').tooltip('show');
			return;
		}
		if (!passNewRepeat)
		{
			$('#pass-new-repeat').tooltip({trigger:'manual', placement:'bottom', title:$BABEL_STR_CANT_BE_EMPTY});
			$('#pass-new-repeat').tooltip('show');
			return;
		}
		if (passNew !== passNewRepeat)
		{
			$('#pass-new-repeat').tooltip({trigger:'manual', placement:'bottom', title:$BABEL_STR_PASSWORD_DOESNT_MATCH});
			$('#pass-new-repeat').tooltip('show');
			return;
		}
		//

		$.post($SCRIPT_ROOT + '/_set_user_password', 'pass_old='+passOld+'&pass_new='+passNew, function(data){
			check_server_data(data);

			if (data['success'])
			{
				$('#admin-pass-old,#admin-pass-new,#admin-pass-new-repeat').val('');
				bootbox.dialog({
					message: "<p class='text-center' style='color:#008000;'><i class='fa fa-check'></i> "+$BABEL_STR_PASSWORD_CHANGED_SUCCESSFULLY+"</p>",
					title: $BABEL_STR_CONFIGURATION_CHANGED,
					buttons: {
						success: {
							label: $BABEL_STR_OK,
							className: "btn-primary",
							callback: function() {
								window.location.href = '/logout';
							}
						}
					}
				});
			}
		});
	});

	// Create User Slot
	$(document).on("click", "#create_user_slot", function(){
		$.post($SCRIPT_ROOT + '/_create_user_slot', '', function(data){
			check_server_data(data);

			if (data['success'] && data['user'])
			{
				var row = "<tr id='user-"+data['user']['id']+"' class='warning'>"+
							"<td><i>"+$BABEL_STR_USER_SLOT+" '"+data['user']['token'].slice(0,10)+"...'</i></td>"+
							"<td class='text-center'><button type='button' data-id='"+data['user']['id']+"' data-demo='false' class='btn btn-outline-danger btn-sm btn-user-demo' style='color:#800000;'><i class='fa fa-minus'></i></button></td>"+
							"<td>"+data['create_date_format']+"</td>"+
							"<td>"+$BABEL_STR_NEVER+"</td>"+
							"<td class='text-right'>"+
							"<button type='button' data-uid='"+data['user']['id']+"' class='btn btn-sm btn-success btn-enable-user hidden' href='http://"+window.location.host+"/user_reg/"+data['user']['token']+"#last-page'>"+
							"<i class='fa fa-user-circle-o'></i> "+$BABEL_STR_ACTIVATE_USER+
							"</button> "+
							"<button type='button' data-uid='"+data['user']['id']+"' class='btn btn-sm btn-success copy-link-user btn-clipboard' data-clipboard-text='http://"+window.location.host+"/user_reg/"+data['user']['token']+"'>"+
							"<i class='fa fa-link'></i> "+$BABEL_STR_COPY_LINK+
							"</button> "+
							"<button type='button' data-uid='"+data['user']['id']+"' class='btn btn-sm btn-primary' data-toggle='modal' data-target='#modal_user_permissions'>"+
							"<i class='fa fa-cog'></i> "+$BABEL_STR_PERMISSIONS+
							"</button> "+
							"<button type='button' class='remove-user btn btn-outline-danger border-0 btn-sm btn-delete-row' data-uid='"+data['user']['id']+"' href='#' title='Cancel User Slot'><i class='fa fa-remove'></i></button>"+
							"</td>";
				$('#tbody-users').append(row);
			}
		});
	});

	// Open Dialog Create Permission Level
	$(document).on("click", "button[data-target='#modal_create_permission_level']", function(){
		$('#form-create-permission-level #name').val('');
		$("#form-create-permission-level input[type='checkbox']").each(function(){ $(this).prop('checked', false); });
	});

	// Press "OK" button in Create Permission Level Dialog
	$(document).on("click", "#modal_create_permission_level .btn-success", function(){
		$.post($SCRIPT_ROOT + '/_create_permission_level', $('#form-create-permission-level').serialize(), function(data) {
			check_server_data(data);

			if (data['success'] && data['perm'])
			{
				var row = "<tr id='permission-"+data['perm']['id']+"'>"+
							"<td>"+data['perm']['name']+"</td>"+
							"<td class='text-center'><button type='button' data-id='"+data['perm']['id']+"' data-perm='start' class='btn btn-sm btn-perm "+(data['perm']['start']?'btn-outline-success':'btn-outline-danger')+"'><i class='fa "+(data['perm']['start']?'fa-check':'fa-minus')+"'></i></button></td>"+
							"<td class='text-center'><button type='button' data-id='"+data['perm']['id']+"' data-perm='stop' class='btn btn-sm btn-perm "+(data['perm']['start']?'btn-outline-success':'btn-outline-danger')+"'><i class='fa "+(data['perm']['stop']?'fa-check success':'fa-minus')+"'></i></button></td>"+
							"<td class='text-center'><button type='button' data-id='"+data['perm']['id']+"' data-perm='config' class='btn btn-sm btn-perm "+(data['perm']['start']?'btn-outline-success':'btn-outline-danger')+"'><i class='fa "+(data['perm']['config']?'fa-check success':'fa-minus')+"'></i></button></td>"+
							"<td class='text-center'><button type='button' data-id='"+data['perm']['id']+"' data-perm='econ' class='btn btn-sm btn-perm "+(data['perm']['start']?'btn-outline-success':'btn-outline-danger')+"'><i class='fa "+(data['perm']['econ']?'fa-check success':'fa-minus')+"'></i></button></td>"+
							"<td class='text-center'><button type='button' data-id='"+data['perm']['id']+"' data-perm='issues' class='btn btn-sm btn-perm "+(data['perm']['start']?'btn-outline-success':'btn-outline-danger')+"'><i class='fa "+(data['perm']['issues']?'fa-check success':'fa-minus')+"'></i></button></td>"+
							"<td class='text-center'><button type='button' data-id='"+data['perm']['id']+"' data-perm='log' class='btn btn-sm btn-perm "+(data['perm']['start']?'btn-outline-success':'btn-outline-danger')+"'><i class='fa "+(data['perm']['log']?'fa-check success':'fa-minus')+"'></i></button></td>"+
							"<td class='text-right'>"+
							"<button type='button' class='remove-permission-level btn btn-outline-danger border-0 btn-sm btn-delete-row' data-id='"+data['perm']['id']+"' href='#' title='"+$BABEL_STR_REMOVE_PERMISSION_LEVEL+"'><i class='fa fa-remove'></i></button>"+
							"</td>"+
							"</tr>";
				$('#tbody-permission-levels').append(row);
				$('#modal_create_permission_level').modal('hide');
				$('#modal_user_permissions select').append("<option value='"+data['perm']['id']+"'>"+data['perm']['name']+"</option>");
			}
		});
	});


	// Change Permission inline
	$(document).on('click', '.btn-perm', function(){
		var $this = $(this);
		var id = $this.data('id');
		var perm = $this.data('perm');

		toggle_perm_icon($this.children('i'));
		$.when($.post($SCRIPT_ROOT + '/_change_permission_level', `id=${id}&perm=${perm}`))
		.then(function(data){
			check_server_data(data);

			if (!data['success'])
				toggle_perm_icon($this.children('i'));
		}).fail(function(){
			toggle_perm_icon($this.children('i'));
		});
	});

	// Remove Permission Level
	$(document).on("click", ".remove-permission-level", function(){
		var permid = $(this).data('id');

		bootbox.dialog({
			message: $BABEL_STR_ARE_YOU_SURE,
			title: $BABEL_STR_DELETE+" "+$BABEL_STR_PERMISSION_LEVEL,
			buttons: {
			    success: {
			    	label: $BABEL_STR_DELETE,
			    	className: "btn-danger",
			    	callback: function() {
						$.post($SCRIPT_ROOT + '/_remove_permission_level/'+permid, '', function(data){
							check_server_data(data);

							if (data['success'])
							{
								$('#permission-'+permid).remove();
								$('#modal_user_permissions select').children("option[value='"+permid+"']").remove();
							}
						});
			    	}
			    },
			    main: {
			    	label: "Cancel",
			    	className: "btn-primary"
			    }
			}
		});
	});

	// Remove User
	$(document).on("click", ".remove-user", function(){
		var uid = $(this).data('uid');

		bootbox.dialog({
			message: $BABEL_STR_ARE_YOU_SURE,
			title: $BABEL_STR_DELETE+" "+$BABEL_STR_USER,
			buttons: {
			    success: {
			    	label: $BABEL_STR_DELETE,
			    	className: "btn-danger",
			    	callback: function() {
						$.post($SCRIPT_ROOT + '/_remove_user/'+uid, '', function(data){
							check_server_data(data);

							if (data['success'])
								$('#user-'+uid).remove();
						});
			    	}
			    },
			    main: {
			    	label: "Cancel",
			    	className: "btn-primary"
			    }
			}
		});
	});

	// Open Dialog User Permissions
	$(document).on("click", "button[data-target='#modal_user_permissions']", function(){
		var uid = $(this).data('uid');

		$('#modal_user_permissions #uid').val(uid);

		$.post($SCRIPT_ROOT + '/_get_user_servers_level/'+uid, '', function(data){
			check_server_data(data);

			if (data['success'])
			{
				$("#modal_user_permissions select").each(function(){
					$(this).val('-1').trigger('change.select2');
				});
				for (var i in data['perms'])
					$('#modal_user_permissions #perm-'+data['perms'][i][0]).val(data['perms'][i][1]).trigger('change.select2');
			}
			else
				$('#modal_user_permissions').modal('hide');
		});
	});

	// Change User Server Perm
	$(document).on('change', '#modal_user_permissions select', function(){
		var uid = $('#modal_user_permissions #uid').val();
		var $this = $(this);
		var select_id = $this.attr('id');
		var select_val = $this.val();
		var srvid = $this.data('srvid');

		$.post($SCRIPT_ROOT + '/_set_user_server_level/'+uid+'/'+srvid, 'perm_id='+select_val, function(data){
			check_server_data(data);
		});
	});

	// Change User Type
	console.log("PASA");
	$(document).on('click', '.btn-user-demo', function(){
		var $this = $(this);
		var uid = $this.data('id');
		var to_demo = !($this.data('demo') || false);
		$this.data('demo', to_demo);
		toggle_perm_icon($this.children('i'));
		$.when($.post($SCRIPT_ROOT + '/_change_user_type', `uid=${uid}&isdemo=${to_demo?1:0}`))
		.then(function(data, textStatus, jqXHR){
			check_server_data(data);

			if (!data['success']) {
				to_demo = !to_demo;
				$this.data('demo', to_demo);
				toggle_perm_icon($this.children('i'));
			}
			if (to_demo) {
				$("a.btn-enable-user[data-uid="+uid+"]").removeClass('hidden');
				$("button.copy-link-user[data-uid="+uid+"]").addClass('hidden');
			} else {
				$("a.btn-enable-user[data-uid="+uid+"]").addClass('hidden');
				$("button.copy-link-user[data-uid="+uid+"]").removeClass('hidden');
			}
		}).fail(function(){
			to_demo = !to_demo;
			$this.data('demo', to_demo);
			toggle_perm_icon($this.children('i'));
			if (to_demo) {
				$("a.btn-enable-user[data-uid="+uid+"]").removeClass('hidden');
				$("button.copy-link-user[data-uid="+uid+"]").addClass('hidden');
			} else {
				$("a.btn-enable-user[data-uid="+uid+"]").addClass('hidden');
				$("button.copy-link-user[data-uid="+uid+"]").removeClass('hidden');
			}
		});
	});

	// Open Dialog New Password
	$(document).on("click", "button[data-target='#modal_new_password']", function(){
		var uid = $(this).data('uid');

		$('#modal_new_password #uid').val(uid);
	});

	// Press "OK" button in New Password Dialog
	$(document).on("click", "#modal_new_password .btn-success", function() {
		var uid = $('#modal_new_password #uid').val();
		var passwd = $('#modal_new_password #passwd').val();

		$.post($SCRIPT_ROOT + '/_change_user_password', `uid=${uid}&passwd=${passwd}`, function(data) {
			check_server_data(data);

			var msg = false;
			if (data['success'])
			{
				$('#modal_new_password').modal('hide');
				bootbox.dialog({
					message: $BABEL_STR_PASSWORD_CHANGED_SUCCESSFULLY,
					title: $BABEL_STR_CHANGE_USER_PASSWORD,
					buttons: {
					    success: {
					    	label: $BABEL_STR_OK,
					    	className: "btn-primary",
					    }
					}
				});
			}
		});
	});

});

function toggle_perm_icon(elm)
{
	if (elm.hasClass('fa-check'))
		elm.removeClass('fa-check btn-outline-success').addClass('fa-minus btn-outline-danger');
	else
		elm.removeClass('fa-minus btn-outline-danger').addClass('fa-check btn-outline-success');
}
