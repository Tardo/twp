#!/usr/bin/env python
# -*- coding: utf-8 -*-
##############################################################################
#    Teeworlds Web Panel
#    Copyright (C) 2016-2017  Alexandre Díaz
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
##############################################################################
import prctl
import signal
from twp import create_app
from twpl.models import db_init
from twpl.configs import TWPConfig
from logging.handlers import RotatingFileHandler
import logging
logging.basicConfig()

prctl.prctl(prctl.NAME, "twp")
prctl.prctl(prctl.PDEATHSIG, signal.SIGTERM)

app = create_app(TWPConfig())
db_init(app)

if len(app.config['LOGFILE']) > 0:
    handler = RotatingFileHandler(app.config['LOGFILE'], maxBytes=app.config['LOGBYTES'], backupCount=1)
    handler.setLevel(logging.DEBUG)
    app.logger.addHandler(handler)

if __name__ == "__main__":
    if app.config['SSL']:
        from OpenSSL import SSL
        context = SSL.Context(SSL.SSLv23_METHOD)
        context.use_privatekey_file(app.config['PKEY'])
        context.use_certificate_file(app.config['CERT'])
        app.run(host=app.config['HOST'], port=app.config['PORT'],
                threaded=app.config['THREADED'], ssl_context=context)
    else:
        app.run(host=app.config['HOST'], port=app.config['PORT'], threaded=app.config['THREADED'])
#     shutdown_all_server_instances()
